<!--
Copyright 2021 Felix Bertoni

SPDX-License-Identifier: MIT
-->

# CMake LLVM Coverage

A simple utility to register tests and generate LLVM report over multiple test executables/executions.

<div class="panel panel-danger">
**Danger**
{: .panel-heading}
<div class="panel-body">

For now, I only develop and test this utility through my other projects, so I can't guarantee any consistency with its API. Also, for now, coverage is only confirmed to work on Linux, not on other platforms, however test handling without coverage should work portably. Regarding portability, merge requests are highly welcome.

Note that this readme may be outdated ! If you want examples of uses, please see my testing framework project, fatigue : [https://gitlab.com/feloxyde/fatigue](https://gitlab.com/feloxyde/fatigue).
</div>
</div>

# Installation

# Registering tests
For this you have two possibilities. 

The first one is to use ```ADD_COVERED_TEST```, allowing to register a single test. 

```cmake
ADD_COVERED_TEST(test_name CMD command [SUITE suite_name])
```

It shall be used alike if you were using a simple ```add_test``` from CMake, except that you can eventually add the test to a suite. The simplest way could look like this : 

```cmake 
add_executable(example example.cpp)
ADD_COVERED_TEST(test_example_1 CMD ${CMAKE_RUNTIME_OUTPUT_DIR}/example)
```

It will create a test called ```test_example_1```, running executable example, and add it to CTest.

We can eventually add the test to a suite. To do so, we need to declare it. 

```cmake 
ADD_TEST_SUITE(suite_name)
```

This creates a suite called ```suite_name```, adding two new targets ```suite_name_testr``` to run tests of the suite with CTest, and ```suite_name_testo``` to run tests of the suite with CTest and output on failure option on.

If we want to add out test to a suite, we can do it like this :

```cmake 
ADD_TEST_SUITE(mysuite)
add_executable(example example.cpp)
ADD_COVERED_TEST(test_example_1 CMD ${CMAKE_RUNTIME_OUTPUT_DIR}/example SUITE suite_name)
```

This will create the suite ```mysuite```, and ```test_example_1``` test will prefixed with this name, so it can be called by ```mysuite_testo`` and ```mysuite_testr``` targets : ```mysuite_test_example_1```.

Finally, we have one more utility for registering tests, which is ```BUILD_TESTS_TO_SUITE```.

```cmake
BUILD_TESTS_TO_SUITE(suite_name TESTS files SOURCE files LIBS targets)
```

It allows to build in one go tests from a list of main files, and add them to a suite. As example :

```cmake
set(srcs 
    source_1.cpp
    source_2.cpp
    source_3.cpp
)

set(tests
    test_main1.cpp
    test_main2.cpp
)

ADD_TEST_SUITE(mysuite)

BUILD_TESTS_TO_SUITE(mysuite 
    TESTS ${tests}
    SOURCES ${srcs}
    LIBS some_lib
)
```

This will result in creating ```mysuite_test_main1``` and ```mysuite_test_main1``` executables, as well as tests with same name. Executable creation is as the following example for ```test_main1.cpp```

```cmake
add_executable(mysuite_test_main1 test_main1.cpp source_1.cpp source_2.cpp source_3.cpp)
target_link_libraries(my_suite_test_main1 some_lib)
```

Please note that ```BUILD_TESTS_TO_SUITE``` positions files relatively to current source directory, which means you can call it in sub directories without worriying about other elements.


# Coverage

To enable coverage, we we need few additional tools. 

- **clang** or **clang++** as compiler to compile with clang coverage
- **llvm-profdata** to process raw coverage files
- **llvm-cov** to generate report
- **llvm-cxxflit** to demangle names, especially templates ones

All these tools shall be in the PATH and invokable as-is.
```LLVM_ENABLE_COVERAGE``` option has to be set to ```ON``` to enable coverage. Additionally, ```ENABLE_LLVM_COVERAGE_INSTANTIATION``` can be set to ```ON``` to display extensive reports about coverage of various instances of each template. 

We then need to force compiler to be clang/clang++, and also to compile with -fprofile-instr-generate -fcoverage-mapping. It can be done, for example, by adding 

```cmake 

if(ENABLE_LLVM_COVERAGE)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-instr-generate -fcoverage-mapping -O0 -g")
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fprofile-instr-generate -fcoverage-mapping -O0 -g")
endif()

```
in the CMakeLists.txt.

Finally, we want to have CMake with clang compiler :

```cmake
#cmake .. -DENABLE_LLVM_COVERAGE=ON -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang
```

Now we are done with the setup, tests registered with either ```ADD_COVERED_TEST``` or ```BUILD_TESTS_TO_SUITE``` will produce a ```.profraw``` file in a ```llvmcoverage``` directory in the build directory. The file is named after the test name, so ```test1``` will result in ```test1.profraws```. 

Then, to exploit them, we can use ```ADD_COVERAGE_REPORT(report_name BIN binary COVERING sources TESTS names BINARIES binaries SOURCES sources)``` to setup coverage generation targets. Report name is name of the target, binary is the covered binary, sources are covered sources and names a list of tests to get coverage from. Example :


```cmake 
ADD_COVERAGE_TARGET(coverage
    BIN somebin
    COVERING ${srcs} ${headers}
    TESTS "*")