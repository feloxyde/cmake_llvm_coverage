# Copyright 2021 Felix Bertoni
#
# SPDX-License-Identifier: MIT

option(ENABLE_LLVM_COVERAGE "Enable llvm coverage. Needs clang++ and/or clang to be selected as compiler, and llvm-profdata, llvm-cov and llvm-cxxflit to be installed and invokable as-is.")
option(ENABLE_LLVM_COVERAGE_INSTANTIATION "Enable reporting llvm coverage with instantiation details, needs ENABLE_LLVM_COVERAGE to be set to ON and proper configuration for it.")


set(LLVM_COVERAGE_OUTPUT_DIR ${CMAKE_BINARY_DIR}/llvmcoverage)

function(ADD_TEST_SUITE suite_name)
    add_custom_target(${suite_name}_testr COMMAND ctest -R "${suite_name}_*")   
    add_custom_target(${suite_name}_testo COMMAND ctest --output-on-failure -R "${suite_name}_*")
    set_property(GLOBAL PROPERTY ${suite_name}_BINARIES_LIST)
endfunction()

function(ADD_BINARY_TO_TEST_SUITE binary suite_name)
    get_property(${suite_name}_BINARIES_LIST GLOBAL PROPERTY ${suite_name}_BINARIES_LIST)
    list(APPEND ${suite_name}_BINARIES_LIST ${binary})
    list(REMOVE_DUPLICATES ${suite_name}_BINARIES_LIST)
    set_property(GLOBAL PROPERTY ${suite_name}_BINARIES_LIST ${${suite_name}_BINARIES_LIST})
endfunction()

function(TEST_SUITE_IMPORT_BINARIES target_suite source_suite)
    get_property(${source_suite}_BINARIES_LIST GLOBAL PROPERTY ${source_suite}_BINARIES_LIST)
    get_property(${target_suite}_BINARIES_LIST GLOBAL PROPERTY ${target_suite}_BINARIES_LIST)
    list(APPEND ${target_suite}_BINARIES_LIST ${${source_suite}_BINARIES_LIST})
    list(REMOVE_DUPLICATES ${target_suite}_BINARIES_LIST)
    set_property(GLOBAL PROPERTY ${target_suite}_BINARIES_LIST ${${target_suite}_BINARIES_LIST})
endfunction()

function(ADD_COVERED_TEST test_name)
    set(options "")
    set(oneValueArgs SUITE)
    set(multiValuesArgs CMD)
    set(ARGP ${test_name}_ARGS)
    cmake_parse_arguments(${ARGP} "${options}" "${oneValueArgs}" "${multiValuesArgs}" ${ARGN}) 
    if(NOT DEFINED ${ARGP}_SUITE)
        set(full_tname "${test_name}")
    else()
        set(full_tname "${${ARGP}_SUITE}_${test_name}")
    endif()
    add_test(NAME ${full_tname} COMMAND ${${ARGP}_CMD})
    if(ENABLE_LLVM_COVERAGE)
        set_property(TEST ${full_tname} APPEND PROPERTY ENVIRONMENT LLVM_PROFILE_FILE=${LLVM_COVERAGE_OUTPUT_DIR}/${full_tname}.profraw)
    endif() 
endfunction()



#this allow to create several tests from C++ sources at once and add them to suite
#params are :  
#TESTS [list] : a list of all test files to add
#SOURCES [list] : a list of all sources that are needed to be used to build with tests
#LIBS [list] a list of all libs that has to be linked to tests
function(BUILD_TESTS_TO_SUITE suite_name)
#parsing arguments
    set(options "")
    set(oneValueArgs "")
    set(multiValuesArgs TESTS SOURCES LIBS)
    set(ARGP ${suite_name}_ARGS)
    cmake_parse_arguments(${ARGP} "${options}" "${oneValueArgs}" "${multiValuesArgs}" ${ARGN})
    #PARSE ALL TEST FILES
    foreach(testf ${${ARGP}_TESTS})
        #creating test executable and linking
        get_filename_component(tname ${testf} NAME_WE)
        set(full_tname "${suite_name}_${tname}")
        #converting sources into correctly presented sources
        foreach(source ${${ARGP}_SOURCES})
            list(APPEND local_source "${CMAKE_CURRENT_SOURCE_DIR}/${source}")
        endforeach()
        add_executable(${full_tname} ${CMAKE_CURRENT_SOURCE_DIR}/${testf} ${local_source})
        ADD_BINARY_TO_TEST_SUITE(${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${full_tname} ${suite_name})
        target_link_libraries(${full_tname} ${${ARGP}_LIBS})
        ADD_COVERED_TEST(${tname} CMD ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${full_tname} SUITE ${suite_name})
    endforeach()
endfunction()


function(ADD_COVERAGE_REPORT report_name)
#parsing arguments
    set(options "")
    set(oneValueArgs "")
    set(multiValuesArgs COVERING TESTS BINARIES)
    set(ARGP ${report_name}_ARGS)
    cmake_parse_arguments(${ARGP} "${options}" "${oneValueArgs}" "${multiValuesArgs}" ${ARGN})
    
    #creating a target to run all tests from the suite
    
    #building up a list of sources 
    
    foreach(cvrd ${${ARGP}_COVERING})
        set(${report_name}_covered ${${report_name}_covered} ${CMAKE_CURRENT_SOURCE_DIR}/${cvrd})
    endforeach()
    
    foreach(tst ${${ARGP}_TESTS})
        set(${report_name}_covering_tests ${${report_name}_covering_tests} ${LLVM_COVERAGE_OUTPUT_DIR}/${tst}.profraw)
    endforeach()

    foreach(bin ${${ARGP}_BINARIES})
        if(NOT ${report_name}_additional_bin)
            set(${report_name}_additional_bin 1)        
        else()
            set(${report_name}_binaries ${${report_name}_binaries} "-object")
        endif()
        set(${report_name}_binaries ${${report_name}_binaries} "${bin}")
    endforeach()
    

    add_custom_target(${report_name}_profdata COMMAND llvm-profdata merge ${${report_name}_covering_tests} -o ${LLVM_COVERAGE_OUTPUT_DIR}/${report_name}.profdata)
    add_custom_target(${report_name}_txt COMMAND llvm-cov report -dump --instr-profile=${LLVM_COVERAGE_OUTPUT_DIR}/${report_name}.profdata  ${${report_name}_binaries} ${${report_name}_covered} > ${LLVM_COVERAGE_OUTPUT_DIR}/${report_name}.txt DEPENDS ${report_name}_profdata)
    if(ENABLE_LLVM_COVERAGE_INSTANTIATION)
        set(llvmcovshowflags --show-line-counts-or-regions --format html --use-color -Xdemangler llvm-cxxfilt --show-instantiations)
    else()
        set(llvmcovshowflags --show-line-counts-or-regions --format html --use-color -Xdemangler llvm-cxxfilt --show-instantiations=false)
    endif()
    add_custom_target(${report_name}_html COMMAND llvm-cov show -dump --instr-profile=${LLVM_COVERAGE_OUTPUT_DIR}/${report_name}.profdata  ${${report_name}_binaries} ${${report_name}_covered} ${llvmcovshowflags} --output-dir=${LLVM_COVERAGE_OUTPUT_DIR}/${report_name}_html DEPENDS ${report_name}_profdata)
endfunction()
